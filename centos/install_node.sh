#!/bin/bash

# https://nodejs.org/en/download/package-manager/
curl --silent --location https://rpm.nodesource.com/setup_8.x | bash -
yum -y install nodejs
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash

