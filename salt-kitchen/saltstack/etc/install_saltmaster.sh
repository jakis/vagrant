#!/bin/bash
# notes: https://www.vultr.com/docs/how-to-install-saltstack-on-centos-7
# https://docs.saltstack.com/en/2015.8/topics/installation/rhel.html
# reference for nonroot: https://docs.saltstack.com/en/latest/ref/configuration/nonroot.html
sudo su

echo "Installing saltmaster..."
yum install wget vim net-tools git GitPython -y

# Add salt repo and install saltstack
cd ~
yum install https://repo.saltstack.com/yum/redhat/salt-repo-latest-1.el7.noarch.rpm -y
yum clean expire-cache -y

# Install master
yum install salt-master -y

# Install minion
yum install salt-minion -y

# Set up master:
cp /app/salt/saltstack/etc/kuali_master /etc/salt/master
cp /app/salt/saltstack/etc/kuali_minion /etc/salt/minion

#mkdir -p /srv/{salt,pillar}
mkdir /srv
cd /srv
ln -s /app/salt/saltstack/salt/ .
ln -s /app/salt/saltstack/pillar/ .

# enable services for master and minion, and then start them and check status
services=(
salt-master.service
salt-minion.service
)

for i in "${services[@]}"; do
  echo item: $i
  systemctl enable $i
  systemctl start $i
  systemctl status $i
done

salt-key -L
