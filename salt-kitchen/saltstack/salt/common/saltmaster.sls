saltmaster:
  user.present:
    - fullname  : Salt Master
    - home      : /home/saltmaster

/etc/sudoers:
  file.append:
    - text:
      - "# Administrators LDAP Group"
      - "saltmaster   ALL=(ALL)       ALL"

salt-master:
  service.running:
    - enable: True

directories:
  file.directory:
    - user: saltmaster
    - group: saltmaster
    - mode: 755 # some permission
    - recurse:
      - user
      - group
    - names:
      - /data
      - /app

/srv/pillar:
  file.symlink:
    - target: /app/ops/salt/saltstack/pillar

/srv/salt:
  file.symlink:
    - target: /app/ops/salt/saltstack/salt

# Owning directories as recommended here:
# https://docs.saltstack.com/en/latest/ref/configuration/nonroot.html

paths:
  file.directory:
    - user: saltmaster
    - group: saltmaster
    - mode: 755 # some permission
    - recurse:
      - user
      - group
    - names:
      - /var/run/salt
      - /var/log/salt
      - /var/cache/salt
      - /etc/salt
