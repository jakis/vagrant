#!/bin/bash

echo I am provisioning...
date > /etc/vagrant_provisioned_at

echo "Installing err..."
apps=(
wget
vim
net-tools
epel-release
python34
)
sudo yum install ${apps[@]} -y

# Set up pip:
sudo curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
sudo python get-pip.py
sudo pip install virtualenv

# make errbot directory:
sudo mkdir /opt/errbot
sudo cp /vagrant/config.py /opt/errbot/config.py

# Create venv:
sudo virtualenv --python /usr/bin/python3 /opt/errbot/venv

# Install errbot within venv
pips=(
errbot
slackclient
)

sudo /opt/errbot/venv/bin/pip install ${pips[@]}














