#!/bin/bash

echo I am provisioning...
date > /etc/vagrant_provisioned_at

echo "Installing rethinkdb..."
sudo yum install wget vim net-tools -y
sudo wget http://download.rethinkdb.com/centos/7/`uname -m`/rethinkdb.repo \
          -O /etc/yum.repos.d/rethinkdb.repo
sudo yum install rethinkdb -y
sudo cp /etc/rethinkdb/default.conf.sample /etc/rethinkdb/instances.d/instance1.conf
