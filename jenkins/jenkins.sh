#!/bin/bash
# notes: https://www.vultr.com/docs/how-to-install-saltstack-on-centos-7
# https://docs.saltstack.com/en/2015.8/topics/installation/rhel.html
# reference for nonroot: https://docs.saltstack.com/en/latest/ref/configuration/nonroot.html
sudo su

echo "Installing jenkins..."
yum install wget vim net-tools git GitPython -y

# Add salt repo and install saltstack
cd ~
wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
yum install jenkins -y
service jenkins start



docker run -d -p 49001:8080 -v $PWD/jenkins:/var/jenkins_home -t jenkins



