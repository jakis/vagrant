#!/bin/python


import rethinkdb

targetserver = '127.0.0.1'
targetport = '28015'

shards = 1
replicas = 2

conn = rethinkdb.connect(targetserver, targetport)
rethinkdb.connect(targetserver, targetport).repl()

print "Listing dbs..."
dblist = rethinkdb.db_list().run(conn)
print dblist

for db in dblist:
    print "Reconfiguring DB %s" % db
    print "Listing tables for DB %s" % db
    tablelist = rethinkdb.db(db).table_list().run(conn)
    print tablelist
    for table in tablelist:
        if db != 'rethinkdb':
            print "Attempting to reconfigure sharding:"
            shardoutput = rethinkdb.db(db).table(table).reconfigure(
                shards=shards,
                replicas=replicas).run(conn)
            print shardoutput















