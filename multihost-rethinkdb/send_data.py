import time
import json
import rethinkdb

server = '127.0.0.1'
serverPort = '28015'
createTables = False

dblist = [
    'aa',
    'bb',
    'cc',
    'dd',
    'ee',
    'ff'
]

conn = rethinkdb.connect(server, serverPort)
rethinkdb.connect(server, serverPort).repl()

if createTables:
    #rethinkdb.db_create("a").run(conn)
    # create the "a" db and the "b" table
    for db in dblist:
        rethinkdb.db_create(db).run(conn)
        rethinkdb.db(db).table_create("b").run()

i = 0

#while i < 1000:
while True:
    epoch_time = int(time.time())
    data = {}
    data['time_val'] = "%s" % epoch_time
    json_data = json.dumps(data)
    print json_data
    for db in dblist:
        rethinkdb.db(db).table("b").insert({
            "time_val": epoch_time
        }).run(conn)
        #time.sleep(.1)
    i += 1

# get all documents from our table and print them
for db in dblist:
    #rethinkdb.db_create(db).run(conn)
    print "Documents for DB: %s" % db
    cursor = rethinkdb.db(db).table("b").run()
    for document in cursor:
        print document
