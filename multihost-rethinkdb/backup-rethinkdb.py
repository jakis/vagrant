#!/usr/local/bin/python

import subprocess
import time
import logging
import os
import sys
import traceback
import kuali.config
from ephemer import json4human

def run(exc):
    """
    Opens subprocess, executes command, and waits for subprocess to exit.
    """
    sub = subprocess.Popen(exc)
    sub.wait()

# def remote_cmd(host, *cmd):
#     """
#     Executes remote command on a given host.
#     """
#     # print "running command %s " % cmd
#     logging.info("Backing up: {host}".format(**locals())
#     run(['vagrant', 'ssh', host, '-c', cmd])

backup_keep = 12

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s [rethink] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

class Status(object):
    errors = 0

def connect_agent():
    fh = open('/data/mgr/coldmetal/agent.conf', 'r+')
    agent_conf = fh.read().rstrip()
    agent_pid, agent_file, agent_ext = agent_conf.split(':')
    os.environ['SSH_AGENT_PID'] = agent_pid
    os.environ['SSH_AUTH_SOCK'] = "/tmp/ssh-%s/agent.%s" % (agent_file, agent_ext)

def setup_env():
    os.environ['ENV'] = "/app/python/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/splunk/bin:/usr/local/bin:/usr/local/sbin:/app/blockip/bin:/app/mgr/bin:/app/python/bin:/home/cmadm/.local/bin:/home/cmadm/bin"
#    os.system("oz index snapshots")

def remote_cmd(host, cmd):
    run(['ssh', '-t', '-t', '-x', host, cmd])

def run(exc):
    # sub = subprocess.Popen(exc)
    # sub.wait()
    print("WHATIF : {}".format(exc))

def backup_rethink(host, backup_root, backup_file):
    epoch_time = int(time.time())
    backup_path = "{backup_root}{epoch_time}-{host}-{backup_file}.tar.gz".format(**locals())
    logging.info("Rethink Dump - {} - {} - {}".format(host,backup_root,backup_file))

def snaprethink(status, host, **kwargs):
    try:
        logging.info("Suspending volumes " + kwargs['volumes'])
        remote_cmd(host, "sudo /app/scripts/bin/vg-suspend " + kwargs['volumes'])
        logging.info("Snapback " + host + " (" + hostname + ")")
        run(['/app/mgr/lib/oz', 'snapback', host, hostname + ':' + str(backup_keep)])
        remote_cmd(host, "sudo /app/scripts/bin/vg-resume " + kwargs['volumes'])
        backup_rethink(host, '/data/rethinkdb/', hostname + '_' + str(backup_keep))
        logging.info("Resuming volumes " + kwargs['volumes'])

    except:
        traceback.print_exc()
        status.errors += 1
        raise

if __name__ == '__main__':
    try:
        hostname = sys.argv[1]
    except IndexError:
        sys.exit("USAGE: " + __file__ + " <hostname>")

    errors = 0
    print("--------------------------------------")
    
    conf = kuali.config.get_config('ops-master')
    print("Loaded config for : {}".format(conf.name))
    
    volumes = "volumes":"vgData/data"

    setup_env()
    connect_agent()
    status = Status()
    snaprethink(status, hostname, volumes=volumes)

    sys.exit(status.errors)

# def backup_rethink(serverlist, backup_root, backup_file):
#     """
#     Connects to remote host via SSH and performs a live backup:
#     e.g. rethinkdb backup -f /path/file.tar.gz

#     Attributes:
#         serverlist: A list of servers to perform backups on.
#         backup_root: The file system path to backup to e.g. /opt/backups
#         backup_file: The desired filename for backups.
#         ** Note: The current epoch time and server name are appended automatically.
#         a backup file of "rethink-backup" will yeild a filename like
#         "1465329295-db1-rethink-backup.tar.gz"
#     """
#     for host in serverlist:
#         epoch_time = int(time.time())
#         backup_path = "{backup_root}{epoch_time}-{host}-{backup_file}.tar.gz".format(**locals())
#         remote_cmd(host, "rethinkdb dump -f {backup_path}".format(**locals())
#         logging.info("Backed up to {backup_path}.".format(**locals())

# backup_rethink(['db1', 'db2'], '/vagrant/backups/', 'rethink-backup')
