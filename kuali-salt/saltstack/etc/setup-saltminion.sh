#!/bin/bash

yum install https://repo.saltstack.com/yum/redhat/salt-repo-latest-1.el7.noarch.rpm -y

yum install salt-minion -y

#create minion config:

mv /etc/salt/minion /etc/salt/minion-original

cat <<EOF > /etc/salt/minion
hash_type: sha256

#master: salt-p1ap1
master: 10.2.0.45
master_finger: '40:1f:2a:ff:b4:b4:e3:e9:59:47:4b:2b:41:ec:c3:db:87:93:9d:0b:88:ca:2b:f5:7f:90:26:78:1b:c6:75:cd'
EOF

# enable services for minion, and then start and check status
services=(
salt-minion.service
)

for i in "${services[@]}"; do
  echo item: $i
  systemctl enable $i
  systemctl start $i
  systemctl status $i
done


