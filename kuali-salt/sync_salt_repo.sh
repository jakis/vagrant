#!/bin/bash

if [ -d "salt_repo" ]; then
  cd salt_repo
  git pull
else
  git clone -b develop --single-branch git@github.com:KualiCo/salt.git ./salt_repo
fi
