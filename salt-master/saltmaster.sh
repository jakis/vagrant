#!/bin/bash
# notes: https://www.vultr.com/docs/how-to-install-saltstack-on-centos-7
# https://docs.saltstack.com/en/2015.8/topics/installation/rhel.html
# reference for nonroot: https://docs.saltstack.com/en/latest/ref/configuration/nonroot.html
sudo su

echo "Installing saltmaster..."
yum install wget vim net-tools git GitPython -y

# Add salt repo and install saltstack
cd ~
yum install https://repo.saltstack.com/yum/redhat/salt-repo-latest-1.el7.noarch.rpm -y
yum clean expire-cache -y

# Install master
yum install salt-master -y

# Install minion
yum install salt-minion -y

# Provision as user "saltmaster"
#chown -R user /etc/salt /var/cache/salt /var/log/salt /var/run/salt

mkdir /app
cd /app
cp -r /vagrant/saltmaster/* /app/salt

# Set up master:
cp /vagrant/kuali_master /etc/salt/master

# Set up localhost minion:
cp /vagrant/kuali_minion /etc/salt/minion

mkdir -p /srv/{salt,pillar}

# enable services for master and minion, and then start them and check status
services=(
salt-master.service
salt-minion.service
)

for i in "${services[@]}"; do
  echo item: $i
  systemctl enable $i
  systemctl start $i
  systemctl status $i
done

salt-key -L
