create errbot_directory:
 file.directory:
   - name: /opt/errbot
   - user: root
   - group: root
   - mode: 755

install_errbot_packages:
  pkg.installed:
    - pkgs:
      - wget
      - 'net-tools'
      - 'epel-release'
      - python34
      - 'vim-common'

# # Installing devtools shouldn't be this hard...
# 'yum groupinstall "Development Tools" --setopt=group_package_types=mandatory,default,optional -y':
#   cmd.run
#
# # "borrowed" from: (but sadly, it doesn't seem to work as well as the cmd.run above.)
# # https://github.com/jcu-eresearch/shared-salt-states/blob/master/development_tools/init.sls
# {% if grains['os_family'] == 'RedHat' %}
# Development Tools:
#   module.run:
#     - name: pkg.group_install
#   {% if grains['osmajorrelease']|int >= 7 %}
#     - m_name: Development Tools
#   {% elif grains['osmajorrelease'] == '6' %}
#     - m_name: Development tools
#   {% endif %}
# {% endif %}
#
# install_pip:
#   pkg.installed:
#     - pkgs:
#       - 'python-pip'
#
# install_virtualenv:
#   pip.installed:
#     - name: virtualenv
#
# create_virtualenv:
#   virtualenv.managed:
#     - python: '/usr/bin/python3'
#     - name: '/opt/errbot/venv'
#
# errbot:
#   pip.installed:
#     - name: errbot
#     - bin_env: '/opt/errbot/venv/bin/pip'
