saltmaster:
  user.present:
    - fullname  : Salt Master
    - home      : /home/saltmaster

/etc/sudoers:
  file.append:
    - text: 
      - "# Administrators LDAP Group"
      - "saltmaster   ALL=(ALL)       ALL"

/srv/salt:
  file.directory:
    - user: saltmaster
    - group: saltmaster
    - mode: 755 # some permission    
    - recurse:
      - user
      - group

# Owning directories as recommended here:
# https://docs.saltstack.com/en/latest/ref/configuration/nonroot.html
/etc/salt:
  file.directory:
    - user: saltmaster
    - group: saltmaster
    - mode: 755 # some permission
    - recurse:
      - user
      - group

/var/cache/salt:
  file.directory:
    - user: saltmaster
    - group: saltmaster
    - mode: 755 # some permission
    - recurse:
      - user
      - group


/var/log/salt:
  file.directory:
    - user: saltmaster
    - group: saltmaster
    - mode: 755 # some permission
    - recurse:
      - user
      - group


/var/run/salt:
  file.directory:
    - user: saltmaster
    - group: saltmaster
    - mode: 755 # some permission
    - recurse:
      - user
      - group
